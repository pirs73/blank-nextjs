/** @type {import('next').NextConfig} */
const path = require('path');
const { TsconfigPathsPlugin } = require('tsconfig-paths-webpack-plugin');

const nextConfig = {
    // reactStrictMode: true,
    swcMinify: true,
    trailingSlash: true,
    webpack: (config) => {
        new TsconfigPathsPlugin({
            configFile: path.resolve(__dirname, './tsconfig.json'),
        });
        config.module.rules.push({
            test: /\.svg$/,
            use: ['@svgr/webpack'],
        });

        return config;
    },
};

module.exports = nextConfig;
