export interface IRadioButtonGroupItem {
    idField: string;
    label: string;
}
