export interface IDropDownListData {
    id: string;
    title: string;
}
