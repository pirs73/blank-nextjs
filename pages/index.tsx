import React from 'react';
import type { NextPage } from 'next';
import Link from 'next/link';
import { useReactiveVar } from '@apollo/client';
import withApollo from '../hoc/withApollo';
import { testReactVar } from '__/cache';
import { BaseLayout } from '__/layouts';

const Home: NextPage = () => {
    const testVar = useReactiveVar(testReactVar);
    return (
        <BaseLayout title="Blank NextJS" index={false}>
            <div className="container">
                <h2>Launches</h2>
                <Link href="/employees/">Сотрудники</Link>
                <br />
                <br />
                <Link href="/ui-kit/">UI Kit</Link>
                <p>test reactiveVar</p>
                <p>{testVar}</p>
                <div id="portalModal"></div>
            </div>
        </BaseLayout>
    );
};

export default withApollo(Home);
