import React from 'react';
import type { NextPage } from 'next';
import {
    ChooseSimplePanel,
    Button,
    ButtonBrand,
    ButtonSecondary,
} from '__/components';
import styles from '__/assets/css/Uikit.module.css';

const UIKit: NextPage = () => {
    return (
        <div className="container b-page">
            <h1>UIKit</h1>
            <ChooseSimplePanel />
            <br />
            <Button className={styles.buttonTree}>button</Button>
            <br />
            <ButtonBrand>button brand</ButtonBrand>
            <br />
            <ButtonSecondary>button secondary</ButtonSecondary>
        </div>
    );
};

export default UIKit;
