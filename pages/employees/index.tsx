import React from 'react';
import type { NextPage } from 'next';
import { BaseLayout, PageLayout } from '__/layouts';

const Employees: NextPage = () => {
    return (
        <BaseLayout title="Сотрудники">
            <PageLayout />
        </BaseLayout>
    );
};

export default Employees;
