import React from 'react';
import type { AppProps } from 'next/app';
import ErrorBoundary from '__/hoc/ErrorBoundary';
import '../assets/css/styles.css';

const MyApp = ({ Component, pageProps }: AppProps): JSX.Element => {
    return (
        <ErrorBoundary>
            <Component {...pageProps} />
        </ErrorBoundary>
    );
};

export default MyApp;
