import React, { useEffect } from "react";
import cn from "classnames";
import { CheckboxRadioButton } from "__/components";
import { IRadioButtonGroupItem } from "__/interfaces";
import { RadioButtonGroupProps } from "./RadioButtonGroup.props";
import styles from "./RadioButtonGroup.module.css";

const RadioButtonGroup = ({
    data = [],
    valueId,
    setRadiobuttonItem,
    className,
    ...props
}: RadioButtonGroupProps) => {
    useEffect(() => {
        let item;
        if (data.length > 0) {
            item = data.filter((i) => i.idField === valueId);
            setRadiobuttonItem(item[0].label);
        }
    }, [data, valueId, setRadiobuttonItem]);

    return (
        <div className={cn(styles.wrapper, className)} {...props}>
            {data.length > 0 &&
                data.map((item: IRadioButtonGroupItem) => (
                    <CheckboxRadioButton
                        key={item.idField}
                        id={item.idField}
                        type="radio"
                        name="radiobutton"
                        checked={item.idField === valueId}
                        label={item.label}
                        value={item.label}
                        onChange={() => setRadiobuttonItem(item.label)}
                        className={styles.radiobutton}
                    />
                ))}
        </div>
    );
};

export { RadioButtonGroup };
