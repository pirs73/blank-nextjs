import { DetailedHTMLProps, HTMLAttributes } from 'react';
import { IRadioButtonGroupItem } from '__/interfaces';

export interface RadioButtonGroupProps
    extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    data: IRadioButtonGroupItem[];
    valueId: string;
    setRadiobuttonItem: (item: string) => void;
}
