import { DetailedHTMLProps, HTMLAttributes } from 'react';
import { IDropDownListData } from '__/interfaces';

export interface ChooseSimpleProps
    extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    data: IDropDownListData[];
}
