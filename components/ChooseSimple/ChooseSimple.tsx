import React, { useEffect, useRef, useState } from 'react';
import cn from 'classnames';
import { DropDownList, Input } from '__/components';
import { ChooseSimpleProps } from './ChooseSimple.props';
import styles from './ChooseSimple.module.css';

type T = any;

const ChooseSimple = ({ data, className, ...props }: ChooseSimpleProps) => {
    const [openDropDown, setOpenDropDown] = useState<boolean>(false);
    const [blockWidth, setBlockWidth] = useState(0);
    const [currentValue, setCurrentValue] = useState<string | null>(null);
    const currentElement: React.MutableRefObject<T | null> = useRef(null);

    useEffect(() => {
        const elWidth =
            currentElement &&
            currentElement.current &&
            currentElement.current.offsetWidth;
        setBlockWidth(elWidth / 2 - 3);
    }, [currentElement]);

    const handleOpenDropDown = (e: any) => {
        e.stopPropagation();
        setOpenDropDown(true);
    };

    const handleCloseDropDown = (e: any) => {
        e.stopPropagation();
        setOpenDropDown(false);
    };

    // function closeDropDown(e: Event) {
    //     e.stopPropagation();
    //     setOpenDropDown(false);
    // }

    // const isBrowser = typeof window !== 'undefined';

    // if (isBrowser && openDropDown) {
    //     const elBody = window.document.querySelector('.b-page');
    //     elBody?.addEventListener('click', (e) => closeDropDown(e));
    // }

    const buttonBlock = () => (
        <div className={styles.buttonsBlock}>
            <button
                className={cn('turbo-button', styles.button)}
                onClick={() => setCurrentValue(null)}
            >
                <i
                    className={cn('font-icon circle-close', styles.iconClose)}
                ></i>
            </button>
            <button
                className={cn('turbo-button', styles.button)}
                onClick={(e) => handleOpenDropDown(e)}
            >
                <i
                    className={cn(
                        'font-icon drop-down-arrow',
                        styles.iconArrow,
                    )}
                ></i>
            </button>
        </div>
    );

    return (
        <div
            ref={currentElement}
            className={cn(styles.wrapper, className)}
            {...props}
        >
            <Input
                label="Test ChooseSimple"
                easy={false}
                buttonBlock={buttonBlock()}
                handleOpenDropDown={handleOpenDropDown}
                currentValue={currentValue ? currentValue : ''}
            />
            {openDropDown && blockWidth > 0 && (
                <DropDownList
                    blockWidth={blockWidth}
                    data={data}
                    setCurrentValue={setCurrentValue}
                    currentValue={currentValue ? currentValue : ''}
                    handleCloseDropDown={handleCloseDropDown}
                />
            )}
        </div>
    );
};

export { ChooseSimple };
