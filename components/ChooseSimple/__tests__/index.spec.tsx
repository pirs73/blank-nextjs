import React from 'react';
import { render, screen } from '@testing-library/react';
import { ChooseSimple } from '__/components';

/**
 * @jest-environment jsdom
 */

describe('ChooseSimple', () => {
    it('render ChooseSimple', () => {
        render(<ChooseSimple data={[{ id: 'id', title: 'title' }]} />);
        screen.debug();
    });
});
