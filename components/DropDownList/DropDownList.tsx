import React from 'react';
import cn from 'classnames';
import { DropDownListProps } from './DropDownList.props';
import styles from './DropDownList.module.css';

const DropDownList = ({
    blockWidth,
    data = [],
    currentValue,
    handleCloseDropDown,
    setCurrentValue,
    className,
    ...props
}: DropDownListProps) => {
    return (
        <div className={cn(styles.block, className)} {...props}>
            <span
                className={styles.sideTop}
                style={{ transform: `translateX(${blockWidth}px)` }}
            ></span>
            <div className={styles.container}>
                {data.length > 0 &&
                    data.map((i) => (
                        <div
                            key={i.id}
                            onClick={(e) => {
                                e.stopPropagation();
                                setCurrentValue(i.title);
                                handleCloseDropDown(e);
                            }}
                            className={cn(styles.listItem, {
                                [styles.orange]: currentValue === i.title,
                            })}
                        >
                            {i.title}
                        </div>
                    ))}
            </div>
        </div>
    );
};

export { DropDownList };
