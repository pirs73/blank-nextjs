import { DetailedHTMLProps, HTMLAttributes } from 'react';
import { IDropDownListData } from '__/interfaces';

export interface DropDownListProps
    extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    blockWidth: number;
    data: IDropDownListData[];
    setCurrentValue: (arg0: string) => void;
    currentValue: string;
    handleCloseDropDown: (e: any) => void;
}
