import React from 'react';
import cn from 'classnames';
import { LabelProps } from './Label.props';
import styles from './Label.module.css';

const Label = ({
    children,
    className,
    htmlFor,
    ...props
}: LabelProps): JSX.Element => {
    return (
        <label
            htmlFor={htmlFor}
            className={cn(className, styles.label)}
            {...props}
        >
            {children}
        </label>
    );
};

export { Label };
