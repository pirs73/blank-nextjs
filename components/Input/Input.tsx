import React, { ForwardedRef, forwardRef, useState } from 'react';
import cn from 'classnames';
import { Label } from '__/components';
import { InputProps } from './Input.props';
import styles from './Input.module.css';

const Input = forwardRef(
    (
        {
            buttonBlock,
            currentValue,
            handleOpenDropDown,
            easy,
            type,
            label,
            name,
            className,
            ...props
        }: InputProps,
        ref: ForwardedRef<HTMLInputElement>
    ): JSX.Element => {
        const [isFocus, setIsFocus] = useState<boolean>(false);

        const handleFocus = (e: any) => {
            e.stopPropagation();
            setIsFocus(true);
            if (handleOpenDropDown) handleOpenDropDown(e);
        };

        return (
            <>
                {label && <Label htmlFor={name}>{label}</Label>}
                {easy ? (
                    <input
                        type={type}
                        id={name}
                        name={name}
                        className={cn(styles.inputEasy, styles.input)}
                        ref={ref}
                        {...props}
                    />
                ) : (
                    <div
                        className={cn(styles.inputWrapper, className, {
                            [styles.inputWrapperFocus]: isFocus,
                        })}
                    >
                        <input
                            type={type}
                            id={name}
                            name={name}
                            className={cn(styles.input)}
                            defaultValue={currentValue}
                            ref={ref}
                            onClick={handleFocus}
                            {...props}
                        />
                        {buttonBlock && buttonBlock}
                    </div>
                )}
            </>
        );
    }
);

export { Input };
