import { DetailedHTMLProps, InputHTMLAttributes, ReactNode } from 'react';

export interface InputProps
    extends DetailedHTMLProps<
        InputHTMLAttributes<HTMLInputElement>,
        HTMLInputElement
    > {
    label?: string;
    buttonBlock?: ReactNode;
    easy: boolean;
    handleOpenDropDown?: (arg0: any) => void;
    currentValue?: string;
}
