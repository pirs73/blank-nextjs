import { DetailedHTMLProps, HTMLAttributes, ReactNode } from 'react';

export interface ModalProps
    extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    children: ReactNode;
    isOpen: boolean;
    onClose: () => void;
    header: string;
    handlerSaveResult: () => void;
    footer: ReactNode;
    bodyClassName?: string;
}
