import React, { useEffect, useMemo } from 'react';
import { createPortal } from 'react-dom';
import cn from 'classnames';
import { Button, ButtonSecondary } from '__/components';
import { ModalProps } from './Modal.props';
import styles from './Modal.module.css';

const isBrowser = typeof window !== 'undefined';

const modalRootElement =
    isBrowser && window.document.querySelector('#modal-root');

const Modal = ({
    isOpen,
    onClose,
    header,
    handlerSaveResult,
    className,
    bodyClassName = '',
    children,
    footer,
    ...props
}: ModalProps) => {
    const element = useMemo(() => {
        const el = document.createElement('div');
        el.classList.add(`${styles.modaleWrapper}`);
        return el;
    }, []);

    useEffect(() => {
        if (isOpen && modalRootElement) {
            modalRootElement?.appendChild(element);

            return () => {
                modalRootElement?.removeChild(element);
            };
        }
    });

    const handlerModalClose = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.stopPropagation();
        const el = window.document.querySelector('html');
        el?.classList.remove('modal-locked');
        onClose();
    };

    if (isOpen) {
        return createPortal(
            <div
                className={cn('modal-portal', className)}
                {...props}
                /** Пример закрытия по клику на пустое место, пока не отлажено */
                // onClick={(e) => handlerModalClose(e)}
            >
                <div className={styles.modalCard}>
                    <div className={styles.header}>
                        {header}
                        <button
                            className={cn(
                                'font-icon cancel',
                                styles.modalCancel,
                            )}
                            onClick={(e) => handlerModalClose(e)}
                        ></button>
                    </div>
                    <div className={cn(styles.modalBody, bodyClassName)}>
                        {children}
                    </div>
                    <div className={cn(styles.modalFooter)}>
                        {footer}
                        <Button
                            className={styles.cancelButton}
                            onClick={(e) => handlerModalClose(e)}
                        >
                            Cancel
                        </Button>
                        <ButtonSecondary
                            className={styles.footerRightButton}
                            onClick={handlerSaveResult}
                        >
                            Create
                        </ButtonSecondary>
                    </div>
                </div>
            </div>,
            element,
        );
    }

    return null;

    /** Пример фунции вызова */
    // const handleOpenModale = () => {
    //     const el = window.document.querySelector("html");
    //     el?.classList.add("modal-locked");
    //     setIsOpen(true);
    // };
    /** Пример вызова компонента */
    /* <Modal
                isOpen={isOpen}
                onClose={() => setIsOpen(false)}
                header="Title"
                bodyClassName={styles.modalBody}
                handlerSaveResult={handlerSaveResult}
                footer={<TestFooterModale />}
            >
                test
                <br /> test <br /> test <br /> test
                <br /> test <br /> test <br /> test
                <br /> test <br /> test <br /> test
                <br /> test <br /> test <br /> test
                <br /> test <br /> test <br /> test
                <br /> test <br /> test <br /> test
                <br /> test <br /> test <br /> test
                <br /> test <br /> test <br /> test
            </Modal> */
};

export { Modal };
