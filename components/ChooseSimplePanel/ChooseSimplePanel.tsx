import React from 'react';
import { ChooseSimple, Input } from '__/components';

/** Компонент для примера использования Input
 *  @param {string} label - Текст для тега label
 *  @param {boolean} easy - Указывает простой компонент Input или с    кнопками и прочим содержимым
 */

const data = [
    { id: '1', title: 'Текст' },
    { id: '2', title: 'Текст 2' },
    { id: '3', title: 'Текст 3' },
    { id: '4', title: 'Текст 4' },
    { id: '5', title: 'Текст 5' },
    { id: '6', title: 'Текст 6' },
    { id: '7', title: 'Текст 7' },
    { id: '8', title: 'Текст 8' },
    { id: '9', title: 'Текст 9' },
    { id: '10', title: 'Текст 10' },
    { id: '11', title: 'Текст 11' },
    { id: '12', title: 'Текст 12' },
    { id: '13', title: 'Текст 13' },
    { id: '14', title: 'Текст 14' },
    { id: '15', title: 'Текст 15' },
    { id: '16', title: 'Текст 16' },
    { id: '17', title: 'Текст 17' },
    { id: '18', title: 'Текст 18' },
    { id: '19', title: 'Текст 19' },
    { id: '20', title: 'Текст 20' },
];

const ChooseSimplePanel = () => {
    return (
        <div>
            <Input label="Easy Input" easy={true} />
            <br />
            <br />
            <ChooseSimple
                data={data}
                // fieldView=""
                // fieldsSearch=""
                // fieldKey=""
                // countShowItem=""
                // onRemove=""
                // isCanEmpty=""
                // value=""
                // trigger=""
                // isEdit=""
                // className=""
                // isButton=""
                // buttonClassNames=""
                // iconColorClass=""
                // disabled={false}
                // extraButtons=""
                // isFullHeight=""
            />
        </div>
    );
};

export { ChooseSimplePanel };
