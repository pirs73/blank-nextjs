import React, { useState } from 'react';
import { CheckboxRadioButton, RadioButtonGroup } from '__/components';
import { IRadioButtonGroupItem } from '__/interfaces';
import styles from './CheckboxPanel.module.css';

/** Пример использования CheckboxRadioButton, RadioButtonGroup */

const data: IRadioButtonGroupItem[] = [
    { idField: '1', label: 'Формировать шапку документа' },
    {
        idField: '2',
        label: 'Формировать преамбулу в начале документа',
    },
    {
        idField: '3',
        label: 'Формировать блок реквизитов в конце документа',
    },
    { idField: '4', label: 'Шаблон c воркфлоу' },
];

const CheckboxPanel = () => {
    const [radiobuttonItem, setRadiobuttonItem] = useState<string | null>(null);

    return (
        <div className={styles.wrapper}>
            <p>Checkbox</p>
            <CheckboxRadioButton
                type="checkbox"
                id="status1"
                name="status1"
                label="Формировать шапку документа"
                value="Формировать шапку документа"
                checked={true}
            />
            <CheckboxRadioButton
                type="checkbox"
                id="status2"
                name="status2"
                label="Формировать преамбулу в начале документа"
                value="Формировать преамбулу в начале документа"
                checked={true}
            />
            <CheckboxRadioButton
                type="checkbox"
                id="status3"
                name="status3"
                label="Формировать блок реквизитов в конце документа"
                value="Формировать блок реквизитов в конце документа"
                checked={true}
            />
            <CheckboxRadioButton
                type="checkbox"
                id="status4"
                name="status4"
                label="Шаблон c воркфлоу"
                value="Шаблон c воркфлоу"
                checked={false}
            />
            <hr />
            <form>
                <p>RadioButtons</p>
                <CheckboxRadioButton
                    type="radio"
                    id="status5"
                    name="radiobutton"
                    value="Формировать шапку документа"
                    label="Формировать шапку документа"
                    checked={true}
                />
                <CheckboxRadioButton
                    type="radio"
                    id="status6"
                    name="radiobutton"
                    value="Формировать преамбулу в начале документа"
                    label="Формировать преамбулу в начале документа"
                />
                <CheckboxRadioButton
                    type="radio"
                    id="status7"
                    name="radiobutton"
                    value="Формировать блок реквизитов в конце документа"
                    label="Формировать блок реквизитов в конце документа"
                />
                <CheckboxRadioButton
                    type="radio"
                    id="status8"
                    name="radiobutton"
                    value="Шаблон c воркфлоу"
                    label="Шаблон c воркфлоу"
                />
            </form>
            <hr />
            <form>
                <p>RadioButtonGroup</p>
                <RadioButtonGroup
                    data={data}
                    valueId="1"
                    setRadiobuttonItem={setRadiobuttonItem}
                />
                {radiobuttonItem && <p>{radiobuttonItem}</p>}
            </form>
        </div>
    );
};

export { CheckboxPanel };
