import { DetailedHTMLProps, InputHTMLAttributes } from 'react';

export interface CheckboxRadioButtonProps
    extends DetailedHTMLProps<
        InputHTMLAttributes<HTMLInputElement>,
        HTMLInputElement
    > {
    label?: string;
    onSelect?: (item: any) => void;
}
