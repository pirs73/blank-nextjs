import React, { ForwardedRef, forwardRef } from 'react';
import cn from 'classnames';
import { CheckboxRadioButtonProps } from './CheckboxRadioButton.props';
import styles from './CheckboxRadioButton.module.css';

const CheckboxRadioButton = forwardRef(
    (
        {
            label,
            checked,
            className,
            id,
            name,
            type,
            value,
            ...props
        }: CheckboxRadioButtonProps,
        ref: ForwardedRef<HTMLInputElement>
    ) => {
        return (
            <div className={cn(styles.checkboxWrap, className)}>
                <label className={styles.label} htmlFor={id}>
                    <input
                        type={type}
                        className={styles.input}
                        id={id}
                        name={name}
                        value={value}
                        ref={ref}
                        defaultChecked={checked}
                        {...props}
                    />
                    <i
                        className={cn(styles.icon, {
                            [styles.radio]: type === 'radio',
                        })}
                    ></i>
                    <span className={styles.span}>{label}</span>
                </label>
            </div>
        );
    }
);

export { CheckboxRadioButton };
