/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable no-console */
import React, { useState, useRef, useEffect } from 'react';
import cn from 'classnames';
import { Resizable } from 'react-resizable';
import { PageLayoutProps } from './PageLayout.props';
import styles from './PageLayout.module.css';

const PageLayout = ({ className, ...props }: PageLayoutProps) => {
    const [sidebar, setSidebar] = useState(400);
    const [footer, setFooter] = useState(210);
    const [sidebarIcon, setSidebarIcon] = useState('icon-arrow-left');
    const [footerIcon, setFooterIcon] = useState('icon-arrow-bottom');
    const ref = useRef(null);
    const refTop = useRef(null);

    const toggleSidebar = () => {
        if (sidebar !== 0) {
            setSidebar(0);
            setSidebarIcon('icon-arrow-right');
        } else if (sidebar === 0) {
            setSidebar(400);
            setSidebarIcon('icon-arrow-left');
        }
    };

    const toggleFooter = () => {
        if (footer !== 0) {
            setFooter(0);
            setFooterIcon('icon-arrow-top');
        } else if (footer === 0 || footerIcon === 'icon-arrow-top') {
            setFooter(250);
            setFooterIcon('icon-arrow-bottom');
        }
    };

    const onResize = (event, { element, size, handle }) => {
        setSidebar(size.width);
    };

    useEffect(() => {
        const resizeableEle: any = ref.current;
        let styles;
        if (resizeableEle) {
            styles = window.getComputedStyle(resizeableEle);
        }

        let height;
        if (styles) {
            height = parseInt(styles.height, 10);
        }

        let y = 0;

        if (resizeableEle) {
            resizeableEle.style.top = '50px';
        }

        const onMouseMoveTopResize = (event: any) => {
            const dy = event.clientY - y;
            height = height - dy;
            y = event.clientY;

            if (resizeableEle) {
                // resizeableEle.style.height = `${height}px`;
                setFooter(height);
                setFooterIcon('icon-arrow-top');
            }
        };

        const onMouseUpTopResize = (event: any) => {
            document.removeEventListener('mousemove', onMouseMoveTopResize);
        };

        const onMouseDownTopResize = (event: any) => {
            y = event.clientY;
            document.addEventListener('mousemove', onMouseMoveTopResize);
            document.addEventListener('mouseup', onMouseUpTopResize);
        };

        // Add mouse down event addEventListener
        const resizerTop: any = refTop.current;

        resizerTop?.addEventListener('mousedown', onMouseDownTopResize);

        return () => {
            resizerTop?.addEventListener('mousedown', onMouseDownTopResize);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div
            className={cn(styles.pageLayout, className)}
            {...props}
            style={{
                gridTemplateColumns: `20px ${sidebar}px 1px auto`,
                gridTemplateRows: `30px auto ${footer}px 20px`,
            }}
        >
            <div className={cn(styles.header)}>Header</div>
            <div className={styles.sidebarToggle}>
                <button
                    className={cn('turbo-button', styles.button)}
                    onClick={() => toggleSidebar()}
                >
                    <i
                        className={cn(
                            `font-icon ${sidebarIcon}`,
                            styles.iconArrow,
                        )}
                    ></i>
                </button>
            </div>
            <Resizable height={800} width={sidebar} onResize={onResize}>
                <div className={cn(styles.sidebar)}>Папки</div>
            </Resizable>
            <div className={styles.sidebarEvent}></div>
            <div className={styles.body}>Таблица</div>
            <div className={styles.footer} ref={ref}>
                <div className={styles.footerEvent} ref={refTop}></div>
                Описание
            </div>
            <div className={styles.footerToggle}>
                <button
                    className={cn('turbo-button', styles.button)}
                    onClick={() => toggleFooter()}
                >
                    <i
                        className={cn(
                            `font-icon ${footerIcon}`,
                            styles.iconArrow,
                        )}
                    ></i>
                </button>
            </div>
        </div>
    );
};

export { PageLayout };
