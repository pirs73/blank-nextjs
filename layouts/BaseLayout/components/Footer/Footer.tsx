import React from 'react';
import cn from 'classnames';
import { FooterProps } from './Footer.props';
import styles from './Footer.module.css';

const Footer = ({ className, ...props }: FooterProps): JSX.Element => {
    return (
        <footer {...props} className={cn(className, styles.footer)}>
            footer
        </footer>
    );
};

export { Footer };
