import React from 'react';
import cn from 'classnames';
import { HeaderProps } from './Header.props';
import styles from './Header.module.css';

const Header = ({ className, ...props }: HeaderProps): JSX.Element => {
    return (
        <header {...props} className={cn(className, styles.header)}>
            header
        </header>
    );
};

export { Header };
