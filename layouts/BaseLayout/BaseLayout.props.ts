import { DetailedHTMLProps, HTMLAttributes, ReactNode } from 'react';

export interface BaseLayoutProps
    extends DetailedHTMLProps<HTMLAttributes<HTMLElement>, HTMLElement> {
    children: ReactNode;
    title: string;
    description?: string;
    keywords?: string;
    index?: boolean;
}
