import React from 'react';
import Head from 'next/head';
import cn from 'classnames';
import { Header, Footer } from './components';
import { BaseLayoutProps } from './BaseLayout.props';
import styles from './BaseLayout.module.css';

const BaseLayout = ({
    children,
    title = 'Home',
    description,
    keywords,
    className,
    index,
}: BaseLayoutProps): JSX.Element => {
    return (
        <>
            <Head>
                <title>{title}</title>
                <meta charSet="utf-8" />
                <meta
                    name="viewport"
                    content="initial-scale=1.0, width=device-width"
                />
                {!index && <meta name="robots" content="noindex, nofollow" />}
                {description && (
                    <meta name="description" content={description} />
                )}
                {keywords && <meta name="keywords" content={keywords} />}
            </Head>
            <div className={cn(className, styles.baseLayout)}>
                <Header className={styles.header} />
                <div className={styles.body}>{children}</div>
                <Footer className={styles.footer} />
            </div>
        </>
    );
};

export { BaseLayout };
