/* eslint-disable no-console */
import { IEmployees } from '__/interfaces';
export const employeesQueris = {
    employees: async (
        _root: undefined,
        _args: any,
    ): Promise<IEmployees[] | null> => {
        try {
            const res = await fetch(
                'https://jsonplaceholder.typicode.com/users',
                {
                    method: 'GET',
                },
            );

            if (!res || res.status !== 200) {
                return null;
            }

            const data = await res.json();
            return data;
        } catch (error: unknown) {
            console.error(error);
            return null;
        }
    },
};
