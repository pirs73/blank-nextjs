import { gql } from 'apollo-server-express';
import { EmployeesTypes } from './types';

export const typeDefs = gql`
    ${EmployeesTypes}

    type Query {
        employees: [Employees]
    }
`;
