/* eslint-disable no-console */
import express from 'express';
import next from 'next';
import { apolloServer } from './graphql';
import { init } from './middlewares';

const port = parseInt(process.env.PORT || '3000', 10);
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(async () => {
    const server = express();

    init(server);

    const serverApp = apolloServer;
    await serverApp.start();
    serverApp.applyMiddleware({ app: server });

    server.all('*', (req: any, res: any) => {
        return handle(req, res);
    });

    server.listen(port, () => {
        console.log(`> Ready on http://localhost:${port}`);
    });
});
