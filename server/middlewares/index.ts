import { Express } from 'express';
import cors from 'cors';
// import morgan from 'morgan';

export const init = (server: Express): void => {
    if (process.env.NODE_ENV === 'production') {
        /** enable with nginx proxy */
        // server.set('trust proxy', 1);

        const allowedOrigins = [
            `http://localhost:3000`,
            `https://localhost:3000`,
            `https://studio.apollographql.com/`,
            `http:turbo.loc`,
        ];

        server.use(
            cors({
                origin: function (origin, callback) {
                    // allow requests with no origin
                    // (like mobile apps or curl requests)
                    if (!origin) return callback(null, true);
                    if (allowedOrigins.indexOf(origin) === -1) {
                        const msg =
                            'The CORS policy for this site does not ' +
                            'allow access from the specified Origin.';
                        return callback(new Error(msg), false);
                    }
                    return callback(null, true);
                },
                credentials: true,
            }),
        );
    }

    // server.use(morgan('dev'));
};
