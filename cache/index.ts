import { makeVar, InMemoryCache } from '@apollo/client';

export const testReactVar = makeVar<string>('UI Kit');

export const cache = new InMemoryCache({
    typePolicies: {
        Query: {
            fields: {
                testReact: {
                    read() {
                        return testReactVar();
                    },
                },
            },
        },
    },
});
