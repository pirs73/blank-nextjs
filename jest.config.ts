import type { Config } from '@jest/types';

export default async (): Promise<Config.InitialOptions> => {
    return {
        verbose: true,
        clearMocks: true,
        collectCoverage: true,
        roots: ['<rootDir>'],
        moduleFileExtensions: ['js', 'ts', 'tsx', 'json'],
        setupFilesAfterEnv: ['<rootDir>/jestSetup.js'],
        testPathIgnorePatterns: [
            '<rootDir>[/\\\\](build|docs|node_modules|.next|out|dist)[/\\\\]',
            '<rootDir>/pages/index.tsx',
            '<rootDir>/dist/server',
        ],
        transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(ts|tsx)$'],
        testEnvironment: 'jsdom',
        // testURL: 'http://localhost',
        testRegex: '/__tests__/.*\\.(test|spec)\\.tsx?$',
        coverageThreshold: {
            global: {
                branches: 60,
                functions: 60,
                lines: 60,
                statements: 60,
            },
        },
        transform: {
            '^.+\\.(ts|tsx)$': 'babel-jest',
            '.+\\.(svg|css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
                'jest-transform-stub',
        },
        moduleNameMapper: {
            '\\.(css|less|scss)$': 'identity-obj-proxy',
            '^__/(.*)$': '<rootDir>/$1',
        },
    };
};
